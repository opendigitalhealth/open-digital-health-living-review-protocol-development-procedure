source("standardizing_excelFiles.R")

task1 <- as.data.frame(read_xlsx("round-2-1.xlsx", sheet = 1, skip = 1))[-1,]
colnames(task1) = c("path_to_entity", "entity", "entity_description",
                             "data_accessibility",
                             "effectiveness_informativeness",
                             "expected_extraction_clarity",
                             "filter_relevance",
                             "implementation_informativeness",
                             "instantiation_informativeness", "stability",
                             "uniqueness", "confidence_rating")
task1_template_numeric_columns = c("data_accessibility",
                                   "effectiveness_informativeness",
                                   "expected_extraction_clarity",
                                   "filter_relevance",
                                   "implementation_informativeness",
                                   "instantiation_informativeness", "stability",
                                   "uniqueness", "confidence_rating")


task2 <- read_xlsx("round-2-1.xlsx", sheet = 2)
colnames(task2) <- c("criterion", "definition", "weight_example",
                              "weight", "confidence")

task3 <- read_xlsx("round-2-1.xlsx", sheet = 3)
colnames(task3) <- "nr_of_entries"

# columns
## extra column
new_col = round(runif(nrow(task1), min = 0, max = 100), digits = 0)
task1_ec = cbind(task1, new_col)
View(task1_ec)

task1_ec_test = column_check(task1_ec, task1_template)
View(task1_ec_test)

## missing column
task1_mc = task1[-11]
View(task1_mc)

task1_mc_test = column_check(task1_mc, task1_template)

# rows
reference_column_task1 = "entity"
## extra rows
new_row = runif(12, min = 0, max = 100)
task1_er = rbind(task1, new_row)
View(task1_er)

task1_er_test = row_check(task1_er, task1_template, reference_column_task1)
View(task1_er_test)

## duplicate rows
task1_dr = task1
rownames(task1_dr) = seq(length=nrow(task1_dr))
task1_dr[125,] = task1_dr[124,]
View(task1_dr)

task1_dr_test = row_check(task1_dr, task1_template, reference_column_task1)
View(task1_dr_test)

## missing rows
task1_mr = task1[-1,]
View(task1_mr)

task1_mr_test = row_check(task1_mr, task1_template, reference_column_task1)

# numeric values
nrow(task1)
new_num_col = round(runif(124, min = 0, max = 100), 0)
new_num_col[1] = "not_a_number"
task1_nnv = cbind(task1, new_num_col)
View(task1_nnv)

task1_nnv_test = numeric_check(task1_nnv, "new_num_col")
View(task1_nnv_test)

# value ranges
## values not within 0-100 (not in range) (task1)
### over 100 
task1_ovr = task1
task1_ovr[1,12] = 150
task1_ovr = numeric_check(task1_ovr, task1_template_numeric_columns)
View(task1_ovr)

task1_ovr_test = value_check(1, task1_ovr)
View(task1_ovr_test)

### under 100
task1_undr = task1_nnv[-13]
task1_undr[1,12] = -20
View(task1_undr)

task1_undr_test = value_check(1, task1_undr)
View(task1_undr_test)

### incorrect file
task1_incorfile = task2
task1_incorfile_test = value_check(1, task1_incorfile)

## values not sum to 100 (task2)
### over 100
task2_ovr = task2
task2_ovr$weight = c(20, 20, 20, 20, 20, 20, 20, 20, 160)
View(task2_ovr)

task2_ovr_test = value_check(2, task2_ovr)
View(task2_ovr_test)

### under 100
task2_undr = task2
task2_undr$weight = c(2, 2, 2, 2, 2, 2, 2, 2, 16)
View(task2_undr)

task2_undr_test = value_check(2, task2_undr)
View(task2_undr_test)

### incorrect file
task2_incorfile = task1
task2_incorfile_test = value_check(2, task2_incorfile)

## task number input not 1 or 2
value_check(3, task1)
