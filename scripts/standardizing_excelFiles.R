library(readxl)
library(dplyr)
library(googlesheets4)
library(stringr)

# template https://docs.google.com/spreadsheets/d/1q_b6rJxm8X6lqIAhrK7a4taD54IsvG7NZATQb0adEdM/edit#gid=0
task1_template = read_sheet(ss = "1q_b6rJxm8X6lqIAhrK7a4taD54IsvG7NZATQb0adEdM",
                            sheet = "round-2__task-1__entity-rating",
                            skip = 1)[-1,]
colnames(task1_template) = c("path_to_entity", "entity", "entity_description",
                             "data_accessibility",
                             "effectiveness_informativeness",
                             "expected_extraction_clarity",
                             "filter_relevance",
                             "implementation_informativeness",
                             "instantiation_informativeness", "stability",
                             "uniqueness", "confidence_rating")
task1_template = task1_template[!duplicated(task1_template[,"entity"]),]
# task1 reference column: entity

task2_template = read_sheet(ss = "1q_b6rJxm8X6lqIAhrK7a4taD54IsvG7NZATQb0adEdM",
                            sheet = "round-2__task-2__criteria-weighting")
colnames(task2_template) <- c("criterion", "definition", "weight_example",
                              "weight", "confidence")
task2_template = task2_template[!duplicated(task2_template[,"criterion"]),]
# task2 reference column: criterion

task3_template = read_sheet(ss = "1q_b6rJxm8X6lqIAhrK7a4taD54IsvG7NZATQb0adEdM",
                            sheet = "round-3__task-3__no-of-entit")
colnames(task3_template) <- c("nr_of_entries")

# checking columns
column_check = function(data, template){
  template_column_count = ncol(template)
  list_of_colnames = colnames(template)
  if (ncol(data) == template_column_count){
    return(data)
  } else if (ncol(data) > template_column_count){
    column_revised = data.frame(matrix(ncol = template_column_count,
                                       nrow = nrow(data)))
    for (i in 1:template_column_count){
      column_revised[,i] = data[,list_of_colnames[i]]
    }
    colnames(column_revised) <- list_of_colnames
    return(column_revised)
  } else {
    stop("There are Missing Columns")
  }
}

# checking rows
row_check = function(data, template, reference_column_name){
  template_list_of_rowValues = pull(.data = template,
                                    var = reference_column_name)
  list_of_colnames = colnames(template)
  data = data[!duplicated(data[,reference_column_name]),]
  row_count = nrow(template)
  if (nrow(data) >= row_count){
    row_revised = data.frame(matrix(ncol = ncol(data), nrow = row_count))
    for (i in 1:row_count){
      row_revised[i,] = data[data[,reference_column_name] ==
                               template_list_of_rowValues[i],]
    }
    colnames(row_revised) <- list_of_colnames
    return(row_revised)
  } else {
    stop("There are Missing Rows")
  }
}

# task1 numeric columns
task1_template_numeric_columns = c("data_accessibility",
                                   "effectiveness_informativeness",
                                   "expected_extraction_clarity",
                                   "filter_relevance",
                                   "implementation_informativeness",
                                   "instantiation_informativeness", "stability",
                                   "uniqueness", "confidence_rating")

# task2 numeric columns
task2_template_numeric_columns = c("weight", "confidence")

# check numbers
numeric_check = function(data, list_of_colNumeric){
  for (i in list_of_colNumeric){
    if (is.numeric(data %>% pull(i))){ # check to see if there are numeric NA
    } else{
      suppressWarnings(cat(paste0("column: ",
             i,
             " | row: ",
             which(is.na(as.numeric(pull(.data = data, var = i)))),
             " | entry: ",
             data[which(is.na(as.numeric(pull(.data = data, var = i)))), i],
             "\n")))
      data[,i] = gsub("[^0-9]", "", data[,i])
    }
    data[,i] = as.numeric(as.character(data[,i]))
  }
  return(data)
}

# check values
value_check = function(task_num, data){
  if (task_num == 1){
    stopifnot("Wrong File: Not a Task1 File" = ncol(data) == 12)
    for (columns in 4:12){
      for (rows in 1:nrow(data)){
        if (isTRUE(data[rows,columns] < 0)){
          data[rows,columns] = gsub("-", "", data[rows,columns])
        } else if (isTRUE(data[rows,columns] > 100)){
          suppressWarnings(cat(paste0("column: ",
                                      columns,
                                      " | row: ",
                                      rows,
                                      " | entry: ",
                                      data[rows,columns],
                                      "\n")))
        }
      }
    }
  } else if (task_num == 2){
    stopifnot("Wrong File: Not a Task2 File" = ncol(data) == 5)
    if (data[9,4] != 100){
      for (rows in 1:8){
        data[rows,4] = (data[rows,4] / data[9,4]) * 100
      }
      data[9,4] = sum(data[1:8,4])
    }
  } else{
    stop("Task Number is not 1 or 2")
  }
  return(data)
}

# check length of task3 (similar to check rows)
check_length_t3 = function(data){
  task3 = data[["nr_of_entries"]]
  if (length(task3) != 1){
    suppressWarnings(cat(paste0("Issue with File", 
                                "\n")))
  } 
  return(data)
}
